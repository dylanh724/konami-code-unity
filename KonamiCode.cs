using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Up, up, down, down, left, right, left, right, b, a
/// Inspired by the pseudocode @ https://www.reddit.com/r/Unity3D/comments/1asdip/implementing_the_konami_code/
/// </summary>
public class KonamiCode : MonoBehaviour
{
    KeyCode[] konamiCode;
    int currentKeyIndex = 0;
    
    KeyCode currentKeyCode;
    Event e;
    
    // ....................................................................
    void Start()
    {
        konamiCode = new KeyCode[]
        {
            KeyCode.UpArrow, KeyCode.UpArrow,
            KeyCode.DownArrow, KeyCode.DownArrow,
            KeyCode.LeftArrow, KeyCode.RightArrow,
            KeyCode.LeftArrow, KeyCode.RightArrow,
            KeyCode.B, KeyCode.A
        };
    }

    // ....................................................................
    void Update()
    {
        checkKonamiCodeInput();
    }

    // ....................................................................
    private void checkKonamiCodeInput()
    {
        e = Event.current;
        currentKeyCode = e.keyCode;

        if (e.type == EventType.KeyUp && currentKeyCode == konamiCode[currentKeyIndex])
        {
            currentKeyIndex++;
            if (currentKeyIndex >= konamiCode.Length)
                konamiTriggered();
        }
    }

    // ....................................................................
    private void konamiTriggered()
    {
        Debug.Log("@ konamiTriggered :trumpet: Derpppp derp derppp! :tada:");
        currentKeyIndex = 0;
    }
}
